//
//  CategoriesViewController.h
//  News
//
//  Created by Dalia Abd El-Hadi on 2/2/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoriesViewController : UIViewController <UICollectionViewDataSource , UICollectionViewDelegateFlowLayout>

@property(nonatomic, weak) IBOutlet UICollectionView *collectionView;

@end
