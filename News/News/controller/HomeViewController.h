//
//  HomeViewController.h
//  News
//
//  Created by Dalia Abd El-Hadi on 2/8/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController <UITableViewDataSource , UITableViewDelegate>

@property (nonatomic,weak) IBOutlet UITableView* tableView;

@end
