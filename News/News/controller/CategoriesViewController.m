//
//  CategoriesViewController.m
//  News
//
//  Created by Dalia Abd El-Hadi on 2/2/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import "CategoriesViewController.h"
#import "ServerAPI.h"
#import "DataCategory.h"
#import "CategoryCollectionViewCell.h"
#import "UserManager.h"
#import "HomeViewController.h"

@interface CategoriesViewController ()
@property (nonatomic, strong) NSMutableArray* categoriesArray;
@property (nonatomic, strong) NSMutableArray* selectedCategoriesArray;

@end

@implementation CategoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //self.view.backgroundColor =[UIColor redColor];
    

    
    self.collectionView.allowsMultipleSelection = YES;
    [self.collectionView registerNib:[UINib nibWithNibName:@"CategoryCollectionViewCell"
                                                    bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"CategoryCell"];
    
    self.categoriesArray = [NSMutableArray array];
    self.selectedCategoriesArray = [NSMutableArray array];
    [self initNavigationBarViews];
    [ServerAPI getParentCategoriesWithHandlerSuccess:^(id response) {
        [self fillArrayWithData:response];
    } failure:^(NSString *errorString) {
        
    }];
}

- (void) initNavigationBarViews
{
//    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 0.0, 150.0, 44.0)];
//    if (isArticleFlag)
//        [titleLabel setText:@"مقالة"];
//    else
//        [titleLabel setText:@"مراجعة"];
//    [titleLabel setTextAlignment:NSTextAlignmentCenter];
//    [titleLabel setTextColor:[UIColor whiteColor]];
//    [titleLabel setBackgroundColor:[UIColor clearColor]];
//    titleLabel.font = FONT_LIGHT_16;
//    [[self navigationItem]setTitleView:titleLabel];
//    
//    
//    UIButton* menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
//    [menuButton setImage:[UIImage imageNamed:@"sideMenuIcon"] forState:UIControlStateNormal];
//    [menuButton addTarget:self action:@selector(presentRightMenuViewController:) forControlEvents:UIControlEventTouchDown];
//    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
//    [[self navigationItem]setRightBarButtonItem:rightBarButton];
    
    
    UIButton* saveButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    [saveButton setTitle:@"save" forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveButtonClicked) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc]initWithCustomView:saveButton];
    [[self navigationItem]setLeftBarButtonItem:leftBarButton];
}


- (void) fillArrayWithData:(NSMutableArray*) feedsArray
{
    if (![feedsArray isKindOfClass:[NSArray class]])
    {
        return;
    }
    for (int i = 0; i < [feedsArray count]; i++) {
        NSDictionary* feed = [feedsArray objectAtIndex:i];
        DataCategory* categoryObj = [[DataCategory alloc] init];
        categoryObj.categoryID = [[feed valueForKey:@"id"] integerValue];
        categoryObj.image1Url = [feed valueForKey:@"image1"];
        categoryObj.image2Url = [feed valueForKey:@"image2"];
        categoryObj.name = [feed valueForKey:@"name"];
        [self.categoriesArray addObject:categoryObj];
    }
    [self.collectionView reloadData];
}

- (NSString*) getCategoriesIDs
{
    NSString* ids = @"";
    for (int i = 0; i < [self.selectedCategoriesArray count]; i++) {
        DataCategory* categoryObject = [self.selectedCategoriesArray objectAtIndex:i];
        if ([ids length] == 0)
        {
            ids = [ids stringByAppendingString:[NSString stringWithFormat:@"%d",(int)categoryObject.categoryID]];
        }
        else
        {
            ids = [ids stringByAppendingString:[NSString stringWithFormat:@",%d",(int)categoryObject.categoryID]];
        }
    }
    return ids;
}
- (void) saveButtonClicked
{
    [ServerAPI getSelectedSourcesForCategories:[self getCategoriesIDs] success:^(id response) {
        NSMutableDictionary* categories = [NSMutableDictionary dictionary];
        NSMutableArray* array = [NSMutableArray arrayWithObjects:[NSNumber numberWithInt:14],[NSNumber numberWithInt:15], nil];
        DataCategory* cat = [self.selectedCategoriesArray objectAtIndex:0];
        [categories setObject:array forKey:[NSString stringWithFormat:@"%@_%d",cat.name,(int)cat.categoryID]];
        [UserManager shared].selectedSources = categories;
        [[UserManager shared] saveSources];
        
        HomeViewController* homeController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        [self.navigationController pushViewController:homeController animated:YES];
    } failure:^(NSString *errorString) {
        NSLog(@"error");
    }];
//    [UserManager shared].selectedCategories = self.selectedCategoriesArray;
}


#pragma mark - UICollectionView Datasource
// 1
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [self.categoriesArray count];
}
// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}
// 3
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CategoryCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"CategoryCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    cell.categoryObject = [self.categoriesArray objectAtIndex:indexPath.row];
    [cell bindCellData];
    return cell;
}
// 4
/*- (UICollectionReusableView *)collectionView:
 (UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
 {
 return [[UICollectionReusableView alloc] init];
 }*/


#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // TODO: Select Item
    DataCategory* categoryObject = [self.categoriesArray objectAtIndex:indexPath.row];
    categoryObject.isSelected = YES;
    [self.selectedCategoriesArray addObject:categoryObject];
    CategoryCollectionViewCell *cell = (CategoryCollectionViewCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
    [cell selectCell:YES];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Deselect item
    DataCategory* categoryObject = [self.categoriesArray objectAtIndex:indexPath.row];
    categoryObject.isSelected = NO;
    CategoryCollectionViewCell *cell = (CategoryCollectionViewCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
    [cell selectCell:NO];
}

#pragma mark – UICollectionViewDelegateFlowLayout

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    DataCategory* categoryObject = [self.categoriesArray objectAtIndex:indexPath.row];
//    NSString *searchTerm = self.searches[indexPath.section]; FlickrPhoto *photo =
//    self.searchResults[searchTerm][indexPath.row];
//    // 2
//    CGSize retval = photo.thumbnail.size.width > 0 ? photo.thumbnail.size : CGSizeMake(100, 100);
//    retval.height += 35; retval.width += 35; return retval;
    
    return CGSizeMake(100, 100);
}

// 3
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(50, 20, 50, 20);
}
@end
