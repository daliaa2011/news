//
//  RightMenuViewController.m
//  News
//
//  Created by Dalia Abd El-Hadi on 2/1/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import "RightMenuViewController.h"
#import "UserManager.h"
#import "FolderTableViewCell.h"

@interface RightMenuViewController ()
@property (nonatomic,assign) BOOL addSourceMenu;
@property (nonatomic,strong) DataCategory* source;
@end

@implementation RightMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView registerNib:[UINib nibWithNibName:@"CreateFolderTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"createFolderCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"FolderTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"folderCell"];
}

- (void) showAddSourceMenu:(DataCategory*) sourceObj
{
    self.addSourceMenu = YES;
    self.source = sourceObj;
    [self.tableView reloadData];
}
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
#pragma mark - UITableViewDataSources
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.addSourceMenu)
    {
        NSDictionary* sourcesDic = [UserManager shared].selectedSources;
        NSArray* allkeys = [sourcesDic allKeys];
        if (indexPath.row == 0)
        {
            UITableViewCell* cell = [UITableViewCell new];
            cell.backgroundColor = [UIColor clearColor];
            cell.contentView.backgroundColor = [UIColor clearColor];
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.textLabel.numberOfLines = 0;
            //cell.textLabel.
            cell.textLabel.text = [NSString stringWithFormat:@"Add %@ Source to",self.source.name];
            return cell;
        }
        else if (indexPath.row == ([allkeys count]  + 1))
        {
            CreateFolderTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"createFolderCell"];
            cell.delegate = self;
            return cell;
        }
        else
        {
            FolderTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"folderCell"];
            cell.folderName = [allkeys objectAtIndex:indexPath.row - 1];
            cell.delegate = self;
            [cell bindCellData];
            return cell;
        }
    }
    else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menuCell"];
        if (!cell)
        {
            cell = [UITableViewCell new];
        }
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        if (indexPath.row == 0)
            cell.textLabel.text = @"Home";
        else if (indexPath.row == 1)
            cell.textLabel.text = @"Latest";
        else if (indexPath.row == 2)
            cell.textLabel.text = @"Urgent";
        else if (indexPath.row == 3)
            cell.textLabel.text = @"Saved for later";
        else if (indexPath.row == 4)
            cell.textLabel.text = @"Saved for offline reading";
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.addSourceMenu)
    {
        NSDictionary* sourcesDic = [UserManager shared].selectedSources;
        NSArray* allkeys = [sourcesDic allKeys];
        
        if (indexPath.row == 0)
            return 44;
        else if (indexPath.row == ([allkeys count]  + 1))
            return 120;
        else
            return 60;
    }
    else
        return 44.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.addSourceMenu)
    {
        NSDictionary* sourcesDic = [UserManager shared].selectedSources;
        NSArray* allkeys = [sourcesDic allKeys];
        return 2 + [allkeys count];
    }
    else
        return 5;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

#pragma mark - MenuDelegate
- (void) createFolderWithName:(NSString*) folderName
{
    NSMutableDictionary* dic = [UserManager shared].selectedSources;
    if ([dic objectForKey:folderName])
    {
        NSLog(@"error");
    }
    else
    {
        [[UserManager shared].selectedSources setObject:[NSMutableArray arrayWithObject:[NSNumber numberWithInteger:self.source.categoryID]] forKey:folderName];
        [[UserManager shared] saveSources];
        [self.tableView reloadData];
    }
}

- (void) addSourceToFolder:(NSString*) folderName
{
    NSMutableDictionary* dic = [UserManager shared].selectedSources;
    NSMutableArray* array = [dic objectForKey:folderName];
    if (!array)
        array = [NSMutableArray arrayWithObject:[NSNumber numberWithInteger:self.source.categoryID]];
    else
        [array addObject:[NSNumber numberWithInteger:self.source.categoryID]];
    [[UserManager shared] saveSources];
}
@end
