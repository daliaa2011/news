//
//  RightMenuViewController.h
//  News
//
//  Created by Dalia Abd El-Hadi on 2/1/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataCategory.h"
#import "CreateFolderTableViewCell.h"

@interface RightMenuViewController : UIViewController <UITableViewDataSource , UITableViewDelegate , MenuDelegate>

@property (nonatomic,weak) IBOutlet UITableView* tableView;

- (void) showAddSourceMenu:(DataCategory*) sourceObj;
@end
