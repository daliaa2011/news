//
//  SourcesViewController.m
//  News
//
//  Created by Dalia Abd El-Hadi on 2/10/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import "SourcesViewController.h"
#import "ServerAPI.h"
#import "DataCategory.h"
#import "CategoryCollectionViewCell.h"
#import "DataCategory.h"
#import "MFSideMenu.h"
#import "RightMenuViewController.h"
#import "UserManager.h"

@interface SourcesViewController ()
@property (nonatomic , assign) BOOL collectionViewShown;
@property (nonatomic, strong) NSMutableArray* categoriesArray;
@property (nonatomic, strong) NSMutableDictionary* sourcesDic;
@end

@implementation SourcesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initNavigationBarViews];
    self.tableView.hidden = YES;
    self.collectionViewShown = YES;
    [self.collectionView registerNib:[UINib nibWithNibName:@"CategoryCollectionViewCell"
                                                    bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"CategoryCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SourceTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SourceCell"];
    [self.mySourcesTableView registerNib:[UINib nibWithNibName:@"SourceTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SourceCell"];
    
    self.categoriesArray = [NSMutableArray array];
    self.sourcesDic = [NSMutableDictionary dictionary];
    [ServerAPI getParentCategoriesWithHandlerSuccess:^(id response) {
        [self fillArrayWithData:response];
    } failure:^(NSString *errorString) {
        
    }];
}

- (void) initNavigationBarViews
{
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 0.0, 150.0, 44.0)];
        [titleLabel setText:@"Sources"];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setTextColor:[UIColor whiteColor]];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        titleLabel.font = FONT_LIGHT_16;
        [[self navigationItem]setTitleView:titleLabel];
    
    
        UIButton* backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
        [backButton setTitle:@"Back" forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchDown];
        UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc]initWithCustomView:backButton];
        [[self navigationItem]setLeftBarButtonItem:leftBarButton];
}

- (void) fillArrayWithData:(NSMutableArray*) feedsArray
{
    if (![feedsArray isKindOfClass:[NSArray class]])
    {
        return;
    }
    for (int i = 0; i < [feedsArray count]; i++) {
        NSDictionary* feed = [feedsArray objectAtIndex:i];
        DataCategory* categoryObj = [[DataCategory alloc] init];
        categoryObj.categoryID = [[feed valueForKey:@"id"] integerValue];
        categoryObj.image1Url = [feed valueForKey:@"image1"];
        categoryObj.image2Url = [feed valueForKey:@"image2"];
        categoryObj.name = [feed valueForKey:@"name"];
        [self.categoriesArray addObject:categoryObj];
    }
    [self.collectionView reloadData];
}

- (void) fillSourcesWithData:(NSDictionary*) feedsDic
{
    if (![feedsDic isKindOfClass:[NSDictionary class]])
    {
        return;
    }
    [self.sourcesDic removeAllObjects];
    NSArray* allKeys = [feedsDic allKeys];
    for (int i = 0; i < [allKeys count]; i++)
    {
        NSArray* sources = [feedsDic objectForKey:[allKeys objectAtIndex:i]];
        if ([sources isKindOfClass:[NSArray class]])
        {
            NSMutableArray* subSourcesArray = [NSMutableArray array];
            for (int j = 0; j < [sources count]; j++) {
                DataCategory* sourceObj = [[DataCategory alloc] init];
                NSString* sourceString = [sources objectAtIndex:j];
                NSArray* components = [sourceString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"_"]];
                if ([components count] == 2)
                {
                    sourceObj.categoryID = [[components objectAtIndex:0] integerValue];
                    sourceObj.name = [components objectAtIndex:1];
                    [subSourcesArray addObject:sourceObj];
                }
                
            }
            if ([subSourcesArray count] > 0)
            {
                [self.sourcesDic setObject:subSourcesArray forKey:[allKeys objectAtIndex:i]];
            }
        }
    }
    self.tableView.hidden = NO;
    self.collectionView.hidden = YES;
    self.collectionViewShown = NO;
    [self.tableView reloadData];
}

- (void) backButtonClicked
{
    if (self.mySourcesTableView.hidden == NO)
        [self.navigationController popViewControllerAnimated:YES];
    else
    {
        if (self.collectionViewShown)
            [self.navigationController popViewControllerAnimated:YES];
        else
        {
            self.tableView.hidden = YES;
            self.collectionView.hidden = NO;
            self.collectionViewShown = YES;
        }
    }
}

- (IBAction)segmentValueChanged
{
    if (self.mainSegment.selectedSegmentIndex == 0)
    {
        self.mySourcesTableView.hidden = YES;
        if (self.collectionViewShown)
        {
            self.collectionView.hidden = NO;
        }
        else
        {
            self.tableView.hidden = NO;
        }
        
    }
    else
    {
        [self.mySourcesTableView reloadData];
        self.collectionView.hidden = YES;
        self.tableView.hidden = YES;
        self.mySourcesTableView.hidden = NO;
    }
}

#pragma mark - UICollectionView Datasource
// 1
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [self.categoriesArray count];
}
// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}
// 3
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CategoryCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"CategoryCell" forIndexPath:indexPath];
    cell.selectImageView.hidden = YES;
    cell.backgroundColor = [UIColor whiteColor];
    cell.categoryObject = [self.categoriesArray objectAtIndex:indexPath.row];
    [cell bindCellData];
    return cell;
}
// 4
/*- (UICollectionReusableView *)collectionView:
 (UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
 {
 return [[UICollectionReusableView alloc] init];
 }*/


#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // TODO: Select Item
    [self.sourcesDic removeAllObjects];
    [self.tableView reloadData];
    DataCategory* categoryObject = [self.categoriesArray objectAtIndex:indexPath.row];
    [ServerAPI getCategoriesByParent:categoryObject.categoryID WithHandlerSuccess:^(id response) {
        [self fillSourcesWithData:response];
    } failure:^(NSString *errorString) {
        
    }];
}



#pragma mark – UICollectionViewDelegateFlowLayout

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //    DataCategory* categoryObject = [self.categoriesArray objectAtIndex:indexPath.row];
    //    NSString *searchTerm = self.searches[indexPath.section]; FlickrPhoto *photo =
    //    self.searchResults[searchTerm][indexPath.row];
    //    // 2
    //    CGSize retval = photo.thumbnail.size.width > 0 ? photo.thumbnail.size : CGSizeMake(100, 100);
    //    retval.height += 35; retval.width += 35; return retval;
    
    return CGSizeMake(100, 100);
}

// 3
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(50, 20, 50, 20);
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
}
#pragma mark - UITableViewDataSources

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel* label = [[UILabel alloc] init];
    label.textAlignment = NSTextAlignmentRight;
    label.backgroundColor = [UIColor lightGrayColor];
    if (tableView == self.tableView)
    {
        NSArray* allkeys = [self.sourcesDic allKeys];
        label.text = [allkeys objectAtIndex:section];
    }
   else
   {
       NSMutableDictionary* dic = [UserManager shared].selectedSources;
       NSArray* allkeys = [dic allKeys];
       if (allkeys && [allkeys count] > 0)
           label.text = [allkeys objectAtIndex:section];
   }
    return label;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SourceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SourceCell"];
    cell.delegate = self;
    if (tableView == self.tableView)
    {
        NSArray* allkeys = [self.sourcesDic allKeys];
        NSArray* sources = [self.sourcesDic objectForKey:[allkeys objectAtIndex:indexPath.section]];
        DataCategory* source = [sources objectAtIndex:indexPath.row];
        cell.sourceObj = source;
    }
    else
    {
        NSMutableDictionary* dic = [UserManager shared].selectedSources;
        NSArray* allkeys = [dic allKeys];
        NSArray* sources = [dic objectForKey:[allkeys objectAtIndex:indexPath.section]];
        DataCategory* source = [[DataCategory alloc] init];//[sources objectAtIndex:indexPath.row];
        source.name = [NSString stringWithFormat:@"%d",[[sources objectAtIndex:indexPath.row] intValue]];
        cell.sourceObj = source;
    }
    [cell bindCellData];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.tableView)
    {
        NSArray* allkeys = [self.sourcesDic allKeys];
        NSArray* sources = [self.sourcesDic objectForKey:[allkeys objectAtIndex:section]];
        return [sources count];
    }
    else
    {
        NSMutableDictionary* dic = [UserManager shared].selectedSources;
        NSArray* allkeys = [dic allKeys];
        NSArray* sources = [dic objectForKey:[allkeys objectAtIndex:section]];
        return [sources count];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.tableView)
    {
        NSArray* allkeys = [self.sourcesDic allKeys];
        return [allkeys count];
    }
    else
    {
        NSMutableDictionary* dic = [UserManager shared].selectedSources;
        NSArray* allkeys = [dic allKeys];
        return [allkeys count];
    }
}

#pragma mark - SourcesDelegate
- (void) addSource:(id) source
{
    RightMenuViewController* menuController =(RightMenuViewController*) self.menuContainerViewController.rightMenuViewController;
    [menuController showAddSourceMenu:source];
    [self.menuContainerViewController toggleRightSideMenuCompletion:^{
        
    }];
}
@end
