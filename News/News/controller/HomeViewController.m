//
//  HomeViewController.m
//  News
//
//  Created by Dalia Abd El-Hadi on 2/8/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import "HomeViewController.h"
#import "ServerAPI.h"
#import "UserManager.h"
#import "Article.h"
#import "ArticleTableViewCell.h"
#import "MFSideMenu.h"
#import "ArticleViewController.h"
#import "SourcesViewController.h"

@interface HomeViewController ()

@property (nonatomic, strong) NSMutableArray* articlesArray;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.tableView registerNib:[UINib nibWithNibName:@"ArticleTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ArticleCell"];
    
    self.articlesArray = [NSMutableArray array];
    
    [self initNavigationBarViews];
    NSString* ids = [[UserManager shared] getCategoriesIDs];
    [ServerAPI getArticlesByCategory:ids page:1 limit:10 success:^(id response) {
        [self fillArrayWithData:response];
    } failure:^(NSString *errorString) {
        
    }];
}

- (void) fillArrayWithData:(NSMutableArray*) feedsArray
{
    if (![feedsArray isKindOfClass:[NSArray class]])
    {
        return;
    }
    for (int i = 0; i < [feedsArray count]; i++) {
        NSDictionary* feed = [feedsArray objectAtIndex:i];
        Article* article = [[Article alloc] init];
        article.articleID = [[feed valueForKey:@"id"] integerValue];
        article.sourceID = [[feed valueForKey:@"cid"] integerValue];
        article.imageUrl = [feed valueForKey:@"image"];
        article.title = [feed valueForKey:@"title"];
        
        [self.articlesArray addObject:article];
    }
    [self.tableView reloadData];
}

- (void) initNavigationBarViews
{
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 0.0, 150.0, 44.0)];
    [titleLabel setText:@"All"];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    titleLabel.font = FONT_LIGHT_16;
    [[self navigationItem]setTitleView:titleLabel];
    
    
    UIButton* menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    [menuButton setTitle:@"Menu" forState:UIControlStateNormal];
//    [menuButton setImage:[UIImage imageNamed:@"sideMenuIcon"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(presentRightMenuViewController) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    [[self navigationItem]setRightBarButtonItem:rightBarButton];
    
    
    UIButton* leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 90, 30)];
    [leftButton setTitle:@"sources" forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(leftButtonClicked) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
    [[self navigationItem]setLeftBarButtonItem:leftBarButton];
}

- (void) leftButtonClicked
{
    SourcesViewController* sourcesController = [[SourcesViewController alloc] initWithNibName:@"SourcesViewController" bundle:nil];
    [self.navigationController pushViewController:sourcesController animated:YES];
}

- (void) presentRightMenuViewController
{
    [self.menuContainerViewController toggleRightSideMenuCompletion:^{
    }];
}
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Article* _article = [self.articlesArray objectAtIndex:indexPath.row];
    ArticleViewController* articleController = [[ArticleViewController alloc] initWithNibName:@"ArticleViewController" AndArticle:_article];
    [self.navigationController pushViewController:articleController animated:YES];
}
#pragma mark - UITableViewDataSources
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ArticleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ArticleCell"];
    cell.articleObj = [self.articlesArray objectAtIndex:indexPath.row];
    [cell bindCellData];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.articlesArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


@end
