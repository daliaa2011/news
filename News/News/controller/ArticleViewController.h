//
//  ArticleViewController.h
//  News
//
//  Created by Dalia Abd El-Hadi on 2/8/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Article.h"

@interface ArticleViewController : UIViewController

- (id) initWithNibName:(NSString *)nibNameOrNil AndArticle:(Article*) _article;

@property (nonatomic,weak) IBOutlet UILabel* titleLabel;
@property (nonatomic,weak) IBOutlet UIImageView* sourceImageView;
@property (nonatomic,weak) IBOutlet UILabel* sourceLabel;
@property (nonatomic,weak) IBOutlet UIImageView* articleImageView;
@property (nonatomic,weak) IBOutlet UITextView* articleDesTextView;
@property (nonatomic,weak) IBOutlet UILabel* tagsLabel;

@property (nonatomic,weak) IBOutlet NSLayoutConstraint* textViewHeight;


- (IBAction) share;
- (IBAction) visitWebsite;
@end
