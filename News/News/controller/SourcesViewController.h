//
//  SourcesViewController.h
//  News
//
//  Created by Dalia Abd El-Hadi on 2/10/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SourceTableViewCell.h"

@interface SourcesViewController : UIViewController <UICollectionViewDataSource , UICollectionViewDelegateFlowLayout , UITableViewDataSource , UITableViewDelegate , SourcesDelegate>

@property(nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property(nonatomic, weak) IBOutlet UITableView* tableView;
@property(nonatomic, weak) IBOutlet UITableView* mySourcesTableView;
@property(nonatomic, weak) IBOutlet UISegmentedControl* mainSegment;
- (IBAction)segmentValueChanged;
@end
