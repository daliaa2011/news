//
//  ArticleViewController.m
//  News
//
//  Created by Dalia Abd El-Hadi on 2/8/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import "ArticleViewController.h"
#import "ServerAPI.h"
#import "UIImageView+WebCache.h"

@interface ArticleViewController ()
@property (nonatomic,strong) Article* articleObj;
@end

@implementation ArticleViewController

- (id) initWithNibName:(NSString *)nibNameOrNil AndArticle:(Article*) _article
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self)
    {
        self.articleObj = _article;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initNavigationBarViews];
    self.articleDesTextView.scrollEnabled = NO;
    self.titleLabel.text = self.articleObj.title;
    
    [ServerAPI getArticlesByID:self.articleObj.articleID success:^(id response) {
        [self fillArrayWithData:response];
        [self fillViewData];
    } failure:^(NSString *errorString) {
        
    }];
}

- (void) initNavigationBarViews
{
    //    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 0.0, 150.0, 44.0)];
    //    if (isArticleFlag)
    //        [titleLabel setText:@"مقالة"];
    //    else
    //        [titleLabel setText:@"مراجعة"];
    //    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    //    [titleLabel setTextColor:[UIColor whiteColor]];
    //    [titleLabel setBackgroundColor:[UIColor clearColor]];
    //    titleLabel.font = FONT_LIGHT_16;
    //    [[self navigationItem]setTitleView:titleLabel];
    //
    //
    UIView* rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 110, 40)];
    UIButton* saveOfflineButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 40)];
    [saveOfflineButton setTitle:@"offline" forState:UIControlStateNormal];
    [saveOfflineButton addTarget:self action:@selector(saveOfflineButtonClicked) forControlEvents:UIControlEventTouchDown];
    [rightView addSubview:saveOfflineButton];
    
    UIButton* saveLaterButton = [[UIButton alloc] initWithFrame:CGRectMake(60, 0, 50, 40)];
    [saveLaterButton setTitle:@"later" forState:UIControlStateNormal];
    [saveLaterButton addTarget:self action:@selector(saveLaterButtonClicked) forControlEvents:UIControlEventTouchDown];
    [rightView addSubview:saveLaterButton];
//    [rightView sizeToFit];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:rightView];
    [[self navigationItem]setRightBarButtonItem:rightBarButton];
    
    
//    UIButton* saveButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
//    [saveButton setTitle:@"save" forState:UIControlStateNormal];
//    [saveButton addTarget:self action:@selector(saveButtonClicked) forControlEvents:UIControlEventTouchDown];
//    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc]initWithCustomView:saveButton];
//    [[self navigationItem]setLeftBarButtonItem:leftBarButton];
}

- (void) fillArrayWithData:(id) feed
{
    if (![feed isKindOfClass:[NSDictionary class]])
        return;
    self.articleObj.articleID = [[feed valueForKey:@"id"] integerValue];
    if ([feed valueForKey:@"cid"])
        self.articleObj.sourceID = [[feed valueForKey:@"cid"] integerValue];
    if (!self.articleObj.imageUrl)
        self.articleObj.imageUrl = [feed valueForKey:@"image"];
    self.articleObj.title = [feed valueForKey:@"title"];
    self.articleObj.body = [feed valueForKey:@"body"];
    self.articleObj.category = [feed valueForKey:@"cats_name"];
    self.articleObj.dateAdded = [feed valueForKey:@"date_added"];
    self.articleObj.newsUrl = [feed valueForKey:@"news_url"];
    self.articleObj.tags = [feed valueForKey:@"tags_name"];
}

- (void) fillViewData
{
    self.titleLabel.text = self.articleObj.title;
    self.sourceLabel.text = self.articleObj.category;
    self.articleDesTextView.text = self.articleObj.body;
    self.tagsLabel.text = self.articleObj.tags;
    NSString* imageUrl = [NSString stringWithFormat:@"%@%@",@"",self.articleObj.imageUrl];
    if (imageUrl)
        [self.articleImageView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];

    CGSize size = [self.articleDesTextView sizeThatFits:CGSizeMake(self.view.frame.size.width - 40, FLT_MAX)];
    self.textViewHeight.constant = size.height + 20;
    
    [self.view layoutIfNeeded];
}

- (IBAction) share;
{
    
}

- (IBAction) visitWebsite
{
    
}

- (void) saveOfflineButtonClicked
{
    
}

- (void) saveLaterButtonClicked
{
    
}
@end
