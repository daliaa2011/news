//
//  ServerAPI.h
//  myApps
//
//  Created by Dalia Abd El-Hadi on 2/5/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import "SharedDataObject.h"

#define SERVER_ADDRESS @"https://cosmic-descent-775.appspot.com/api/index.php?action="

#define kBackGroudQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

#define PLATFRORM_IDS @"1,4,5"

typedef void (^SuccessBlock) (id response);
typedef void (^FailureBlock) (NSString *errorString);
@interface ServerAPI : NSObject {
    
}

#pragma mark - Requests

+ (void)getArticlesByID:(NSInteger)articleID
                success:(SuccessBlock)success
                failure:(FailureBlock)failure;

+ (void) getParentCategoriesWithHandlerSuccess:(SuccessBlock)success
                                       failure:(FailureBlock)failure;

+ (void) getCategoriesByParent:(NSInteger)parentID WithHandlerSuccess:(SuccessBlock)success
                       failure:(FailureBlock)failure;

+ (void) getArticlesByCategory:(NSString*) categoriesIds
                          page:(NSInteger)pagenum
                         limit:(NSInteger)limit
                       success:(SuccessBlock)success
                       failure:(FailureBlock)failure;

+ (void) getSelectedSourcesForCategories:(NSString*) categoriesIds success:(SuccessBlock)success
                                 failure:(FailureBlock)failure;
@end
