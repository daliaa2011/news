//
//  ServerAPI.m
//  myApps
//
//  Created by Dalia Abd El-Hadi on 2/5/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.

#import "ServerAPI.h"
#import "KeychainManager.h"
#import "AFHTTPRequestOperationManager.h"

@implementation ServerAPI

#pragma mark - requests

+ (void)getArticlesByID:(NSInteger)articleID
                success:(SuccessBlock)success
                failure:(FailureBlock)failure{
    NSString* userID = @"1";//[[KeychainManager sharedInstance].keychainPasswordItem myObjectForKey:(__bridge id)kSecAttrLabel];
    if (!(userID && [userID length] > 0))
        userID = @"0";
    NSString *paramString = [NSString stringWithFormat:@"%@get_article&aid=%d&cid=47&uid=%@",SERVER_ADDRESS,(int)articleID,userID];
    NSString* encodedUrl = [paramString stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:encodedUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(@"Server is not responding properly. Please try after some time");
    }];
}

+ (void) getParentCategoriesWithHandlerSuccess:(SuccessBlock)success
                                 failure:(FailureBlock)failure
{
    NSString *paramString = [NSString stringWithFormat:@"%@get_parent_categories&client_id=47",SERVER_ADDRESS];
    NSString* encodedUrl = [paramString stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:encodedUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(@"Server is not responding properly. Please try after some time");
    }];
}

+ (void) getCategoriesByParent:(NSInteger)parentID WithHandlerSuccess:(SuccessBlock)success
                                       failure:(FailureBlock)failure
{
    NSString *paramString = [NSString stringWithFormat:@"%@get_categories_by_parent&client_id=47&parent=%d",SERVER_ADDRESS,(int)parentID];
    NSString* encodedUrl = [paramString stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:encodedUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(@"Server is not responding properly. Please try after some time");
    }];
}

+ (void) getArticlesByCategory:(NSString*) categoriesIds
                          page:(NSInteger)pagenum
                         limit:(NSInteger)limit
                       success:(SuccessBlock)success
                       failure:(FailureBlock)failure
{
    NSString *paramString = [NSString stringWithFormat:@"%@get_articles_by_cat&cid=%@&client_id=47&page=%d&limit=%d",SERVER_ADDRESS,categoriesIds,(int)pagenum,(int)limit];
    NSString* encodedUrl = [paramString stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:encodedUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(@"Server is not responding properly. Please try after some time");
    }];
}

+ (void) getSelectedSourcesForCategories:(NSString*) categoriesIds success:(SuccessBlock)success
                                 failure:(FailureBlock)failure
{
    success(nil);
}
#pragma mark - Functions
+ (void)sendRequestWithRequestString:(NSString *)requestString
                             success:(SuccessBlock)success
                             failure:(FailureBlock)failure {
    
    BOOL isConnected = [self connected];
    if(!isConnected) {
        [self displayNoAlertMessage];
        
        !failure ? : failure(@"Server is not responding properly. Please try after some time");
    } else {
        
        NSURL *url = [NSURL URLWithString:[requestString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        dispatch_async(kBackGroudQueue, ^{
            
            NSData *data = [NSData dataWithContentsOfURL:url];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSError *error;
                NSDictionary *JSON = nil;
                @try {
                    JSON = [NSJSONSerialization JSONObjectWithData:data
                                                           options:NSJSONReadingMutableLeaves
                                                             error:&error];
                    
                    
                    NSLog(@"Req: %@ \nRes: %@", url, JSON);
                    
                    !success ? : success(JSON);
                }
                @catch (NSException *exception) {
                    !failure ? : failure(@"Server is not responding properly. Please try after some time");
                }
            });
        });
        
    }
}

+ (BOOL)connected {
    Reachability *hostReach = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [hostReach currentReachabilityStatus];
    return netStatus;
}

+ (void)displayNoAlertMessage {
    //dismissActivityIndicator & Show Alert
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"تنبيه"
                                                   message:@"تعذر الإتصال بالإنترنت، تأكد من إعدادات الشبكة ثم حاول مرة أخرى لاحقاً"
                                                  delegate:self
                                         cancelButtonTitle:@"موافق"
                                         otherButtonTitles:nil];
    [alert show];
}
@end
