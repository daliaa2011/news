//
//  DataCategory.h
//  News
//
//  Created by Dalia Abd El-Hadi on 2/7/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataCategory : NSObject

@property (nonatomic , assign) NSInteger categoryID;
@property (nonatomic , strong) NSString* image1Url;
@property (nonatomic , strong) NSString* image2Url;
@property (nonatomic , strong) NSString* name;

@property (nonatomic , assign) BOOL isSelected;
@end
