//
//  Article.h
//  News
//
//  Created by Dalia Abd El-Hadi on 2/8/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Article : NSObject

@property (nonatomic , assign) NSInteger sourceID;
@property (nonatomic , assign) NSInteger articleID;
@property (nonatomic , strong) NSString* imageUrl;
@property (nonatomic , strong) NSString* title;
@property (nonatomic , strong) NSString* body;
@property (nonatomic , strong) NSString* category;
@property (nonatomic , strong) NSString* dateAdded;
@property (nonatomic , strong) NSString* newsUrl;
@property (nonatomic , strong) NSString* sourceName;
@property (nonatomic , strong) NSString* tags;
@end
