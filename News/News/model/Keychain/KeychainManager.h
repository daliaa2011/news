//
//  KeychainManager.h
//  NABuddy
//
//  Created by Dalia Abd El-Hadi on 6/25/14.
//  Copyright (c) 2014 Ahmed Saad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeychainWrapper.h"

@interface KeychainManager : NSObject

@property (nonatomic, strong) KeychainWrapper *keychainPasswordItem;
+ (KeychainManager*) sharedInstance;
- (void) resetKeyChain;
@end
