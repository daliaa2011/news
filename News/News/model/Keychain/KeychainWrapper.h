//
//  KeychainWrapper.h
//  NABuddy
//
//  Created by Dalia Abd El-Hadi on 6/25/14.
//  Copyright (c) 2014 Ahmed Saad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Security/Security.h>

@interface KeychainWrapper : NSObject
{
    NSMutableDictionary        *keychainData;
    NSMutableDictionary        *genericPasswordQuery;
}
@property (nonatomic, strong) NSMutableDictionary *keychainData;
@property (nonatomic, strong) NSMutableDictionary *genericPasswordQuery;
@property (nonatomic, strong) NSString* identifier;

- (id)initWithIdentifier: (NSString *)identifier;
- (void)mySetObject:(id)inObject forKey:(id)key;
- (id)myObjectForKey:(id)key;
- (void)resetKeychainItem;
@end
