//
//  KeychainManager.m
//  NABuddy
//
//  Created by Dalia Abd El-Hadi on 6/25/14.
//  Copyright (c) 2014 Ahmed Saad. All rights reserved.
//

#import "KeychainManager.h"

@implementation KeychainManager

static KeychainManager *sharedSettings = nil;

+ (KeychainManager*)sharedInstance
{
    @synchronized(self) {
        if (sharedSettings == nil) {
            sharedSettings = [[self alloc] init];
        }
    }
    return sharedSettings;
}

- (id) init
{
    self = [super init];
    if (self)

    {
        self.keychainPasswordItem = [[KeychainWrapper alloc] initWithIdentifier:@"password"];
    }
    return self;
}

- (void) resetKeyChain
{
    [self.keychainPasswordItem resetKeychainItem];
    self.keychainPasswordItem = [[KeychainWrapper alloc] initWithIdentifier:@"password"];
}
@end
