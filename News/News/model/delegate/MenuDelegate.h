//
//  MenuDelegate.h
//  News
//
//  Created by Dalia Abd El-Hadi on 2/11/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MenuDelegate <NSObject>
@optional
- (void) createFolderWithName:(NSString*) folderName;
- (void) addSourceToFolder:(NSString*) folderName;
@end
