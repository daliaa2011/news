//
//  UserManager.h
//  News
//
//  Created by Dalia Abd El-Hadi on 2/8/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import <Foundation/Foundation.h>

#define Sources_FILE_NAME       @"sources.plist"

@interface UserManager : NSObject

@property (nonatomic,strong) NSMutableDictionary* selectedSources;

+ (UserManager *) shared;
- (NSString*) getCategoriesIDs;

- (void) loadSources;
- (void) saveSources;
@end
