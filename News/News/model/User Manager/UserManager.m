//
//  UserManager.m
//  News
//
//  Created by Dalia Abd El-Hadi on 2/8/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import "UserManager.h"
#import "DataCategory.h"

static UserManager *_sharedObject = nil;
@implementation UserManager

+ (UserManager *)shared {
    @synchronized(self) {
        if (_sharedObject == nil) {
            _sharedObject = [[self alloc] init];
        }
    }
    return _sharedObject;
}

- (id) init
{
    self = [super init];
    if (self)
    {
        [self loadSources];
    }
    return self;
}
- (NSString*) getCategoriesIDs
{
   // return @"14";
    NSString* ids = @"";
    NSArray* allkeys = [self.selectedSources allKeys];
    for (int i = 0; i < [allkeys count]; i++) {
        NSArray* sources = [self.selectedSources objectForKey:[allkeys objectAtIndex:i]];
        for (int j = 0; j < [sources count]; j++) {
            int sourceID = [[sources objectAtIndex:j] intValue];
            if ([ids length] == 0)
            {
                ids = [ids stringByAppendingString:[NSString stringWithFormat:@"%d",sourceID]];
            }
            else
            {
                ids = [ids stringByAppendingString:[NSString stringWithFormat:@",%d",sourceID]];
            }
        }
    }
    
    return ids;
}

- (void) saveSources
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectoryPath = [paths objectAtIndex:0];
    NSString *myFilePath = [documentsDirectoryPath stringByAppendingPathComponent:Sources_FILE_NAME];
    [self.selectedSources writeToFile:myFilePath atomically:YES];
}

- (void) loadSources
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectoryPath = [paths objectAtIndex:0];
    NSString *myFilePath = [documentsDirectoryPath stringByAppendingPathComponent:Sources_FILE_NAME];
    self.selectedSources = [NSMutableDictionary dictionaryWithContentsOfFile:myFilePath];
}
@end
