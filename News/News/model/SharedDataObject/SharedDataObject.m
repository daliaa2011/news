//
//  SharedDataObject.m
//  myApps
//
//  Created by Dalia Abd El-Hadi on 2/1/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import "SharedDataObject.h"

@implementation SharedDataObject

#pragma mark - Singeltone

+ (SharedDataObject *)shared {
    static SharedDataObject *_sharedObject = nil;
    static dispatch_once_t onceToken;
    dispatch_once (&onceToken, ^{
        _sharedObject = [SharedDataObject new];
    });
    return _sharedObject;
}

+ (float)screenWidth {
    return SCREEN_WIDTH;
}

+ (float)screenHeight {
    return SCREEN_HEIGHT;
}
@end
