//
//  AppDataDelegate.h
//  myApps
//
//  Created by Dalia Abd El-Hadi on 2/1/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AppDataObject;

@protocol AppDataDelegate <NSObject>

- (AppDataObject *)theSharedDataObject;

@end
