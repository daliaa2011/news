//
//  SharedDataObject.h
//  myApps
//
//  Created by Dalia Abd El-Hadi on 2/1/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

#import "AppDataObject.h"
#import "Reachability.h"
#import "ServerAPI.h"

#define SCREEN_WIDTH [[UIScreen mainScreen]applicationFrame].size.width;
#define SCREEN_HEIGHT [[UIScreen mainScreen]applicationFrame].size.height;
#define NAVIGATION_BAR_HEIGHT   64

#define FONT_LIGHT(fontSize) [UIFont fontWithName:@"JFFlat-Regular" size:(CGFloat)fontSize];
#define fontMacro(_name_, _size_) ((UIFont *)[UIFont fontWithName:(NSString *)(_name_) size:(CGFloat)(_size_)]);
#define FONT_LIGHT_10 [UIFont fontWithName:@"JFFlat-Regular" size:10.0];
#define FONT_LIGHT_12 [UIFont fontWithName:@"JFFlat-Regular" size:12.0];
#define FONT_LIGHT_13 [UIFont fontWithName:@"JFFlat-Regular" size:13.0];
#define FONT_LIGHT_15 [UIFont fontWithName:@"JFFlat-Regular" size:15.0];
#define FONT_LIGHT_14 [UIFont fontWithName:@"JFFlat-Regular" size:14.0];
#define FONT_LIGHT_16 [UIFont fontWithName:@"JFFlat-Regular" size:16.0];
#define FONT_LIGHT_18 [UIFont fontWithName:@"JFFlat-Regular" size:18.0];
#define FONT_LIGHT_20 [UIFont fontWithName:@"JFFlat-Regular" size:20.0];


#pragma mark - MACROS :
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]



#define IMAGE_HOST_ADDRESS @"http://apparabi.com/backend/img/video_images/"
#define THUMB_IMAGE_HOST_ADDRESS @"http://apparabi.com/backend/img/video_images_thumbnail/"
#define ARTICLE_THUMB_IMAGE_HOST_ADDRESS @"http://apparabi.com/backend/img/video_images_thumbnail/320x120/"
#define CATEGORIES_IMAGE_HOST_ADDRESS @"http://apparabi.com/backend/img/categories/"
@interface SharedDataObject : AppDataObject {
    
}

@property int typeID;
@property int categoryID;

+ (SharedDataObject *)shared;

+ (float)screenWidth;
+ (float)screenHeight;
@end
