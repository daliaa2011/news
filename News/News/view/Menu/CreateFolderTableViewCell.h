//
//  CreateFolderTableViewCell.h
//  News
//
//  Created by Dalia Abd El-Hadi on 2/11/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuDelegate.h"

@interface CreateFolderTableViewCell : UITableViewCell
@property (nonatomic,weak) id <MenuDelegate> delegate;
@property (nonatomic,weak) IBOutlet UITextField* folderNameTextField;
- (IBAction) okButtonClicked;
- (IBAction) cancelButtonClicked;
@end
