//
//  CreateFolderTableViewCell.m
//  News
//
//  Created by Dalia Abd El-Hadi on 2/11/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import "CreateFolderTableViewCell.h"

@implementation CreateFolderTableViewCell

- (IBAction) okButtonClicked
{
    if ([self.folderNameTextField.text length] > 0)
    {
        [self.delegate createFolderWithName:self.folderNameTextField.text];
        self.folderNameTextField.text = @"";
    }
}

- (IBAction) cancelButtonClicked
{
    self.folderNameTextField.text = @"";
}

@end
