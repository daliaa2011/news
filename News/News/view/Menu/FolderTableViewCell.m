//
//  FolderTableViewCell.m
//  News
//
//  Created by Dalia Abd El-Hadi on 2/11/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import "FolderTableViewCell.h"

@implementation FolderTableViewCell

- (IBAction)buttonClickd
{
    [self.delegate addSourceToFolder:self.folderName];
}

- (void) bindCellData
{
    [self.button setTitle:self.folderName forState:UIControlStateNormal];
}
@end
