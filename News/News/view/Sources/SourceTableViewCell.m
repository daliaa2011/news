//
//  SourceTableViewCell.m
//  News
//
//  Created by Dalia Abd El-Hadi on 2/10/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import "SourceTableViewCell.h"

@implementation SourceTableViewCell

- (void) bindCellData
{
    self.titleLabel.text = self.sourceObj.name;
}

- (IBAction)AddSource
{
    [self.delegate addSource:self.sourceObj];
}
@end
