//
//  SourceTableViewCell.h
//  News
//
//  Created by Dalia Abd El-Hadi on 2/10/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataCategory.h"


@protocol SourcesDelegate <NSObject>

- (void) addSource:(id) source;

@end

@interface SourceTableViewCell : UITableViewCell

@property (nonatomic,strong) DataCategory* sourceObj;
@property (nonatomic,weak) IBOutlet UILabel* titleLabel;
@property (nonatomic,weak) IBOutlet UILabel* desLabel;
@property (nonatomic,weak) IBOutlet UILabel* numOfFollowersLabel;
@property (nonatomic,weak) IBOutlet UIButton* addButton;
@property (nonatomic,weak) id <SourcesDelegate> delegate;
- (IBAction)AddSource;

- (void) bindCellData;
@end
