//
//  CategoryCollectionViewCell.h
//  News
//
//  Created by Dalia Abd El-Hadi on 2/7/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DataCategory;
@interface CategoryCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UIImageView* selectImageView;
@property (nonatomic, strong) DataCategory *categoryObject;

- (void) bindCellData;
- (void) selectCell:(BOOL) isSelected;
@end
