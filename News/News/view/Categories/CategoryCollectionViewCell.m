//
//  CategoryCollectionViewCell.m
//  News
//
//  Created by Dalia Abd El-Hadi on 2/7/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import "CategoryCollectionViewCell.h"
#import "DataCategory.h"
#import "UIImageView+WebCache.h"

@implementation CategoryCollectionViewCell


- (void) bindCellData
{
    if (self.categoryObject.image1Url)
    {
        NSString* imageUrl = [NSString stringWithFormat:@"%@%@",@"",self.categoryObject.image1Url];
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    }
    self.nameLabel.text = self.categoryObject.name;
    [self selectCell:self.categoryObject.isSelected];
}

- (void) selectCell:(BOOL) isSelected
{
    if (isSelected)
    {
        self.selectImageView.backgroundColor = [UIColor greenColor];
    }
    else
    {
        self.selectImageView.backgroundColor = [UIColor whiteColor];
    }
}
@end
