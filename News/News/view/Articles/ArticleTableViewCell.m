//
//  ArticleTableViewCell.m
//  News
//
//  Created by Dalia Abd El-Hadi on 2/8/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import "ArticleTableViewCell.h"
#import "Article.h"
#import "UIImageView+WebCache.h"

@implementation ArticleTableViewCell

- (void) bindCellData
{
    if (self.articleObj.imageUrl)
    {
        NSString* imageUrl = [NSString stringWithFormat:@"%@%@",@"",self.articleObj.imageUrl];
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    }
    self.titleLabel.text = self.articleObj.title;
}
@end
