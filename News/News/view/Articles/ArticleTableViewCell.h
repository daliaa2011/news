//
//  ArticleTableViewCell.h
//  News
//
//  Created by Dalia Abd El-Hadi on 2/8/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Article;
@interface ArticleTableViewCell : UITableViewCell

@property (nonatomic, strong) Article* articleObj;
@property (nonatomic, strong) IBOutlet UIImageView *articleImageView;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

@property (nonatomic, strong) IBOutlet UIImageView *sourceImageView;
@property (nonatomic, strong) IBOutlet UILabel *sourceLabel;

- (void) bindCellData;
@end
