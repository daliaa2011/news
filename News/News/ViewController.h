//
//  ViewController.h
//  News
//
//  Created by Dalia Abd El-Hadi on 2/5/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RightMenuViewController.h"
#import "MFSideMenuContainerViewController.h"

@interface ViewController : UIViewController
{
    MFSideMenuContainerViewController* sideMenuViewController;
    UINavigationController* navigationController;
    RightMenuViewController* rightMenuController;
}

@end

