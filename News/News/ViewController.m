//
//  ViewController.m
//  News
//
//  Created by Dalia Abd El-Hadi on 2/5/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import "ViewController.h"
#import "CategoriesViewController.h"
#import "HomeViewController.h"

#import "MFSideMenu.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    BOOL goToHomeScreen = [[NSUserDefaults standardUserDefaults] boolForKey:@"goToHomeScreen"];
    if (!goToHomeScreen)
    {
        navigationController = [[UINavigationController alloc] initWithRootViewController:[[CategoriesViewController alloc] initWithNibName:@"CategoriesViewController" bundle:nil]];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"goToHomeScreen"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else
    {
        navigationController = [[UINavigationController alloc] initWithRootViewController:[[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil]];
    }
    [navigationController.navigationBar setTranslucent:NO];
    [navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:69.0/255.0 green:58.0/255.0 blue:67.0/255.0 alpha:1]];
    
    rightMenuController = [[RightMenuViewController alloc] initWithNibName:@"RightMenuViewController" bundle:nil];
    // rightMenuController.delegate = self;
    rightMenuController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    sideMenuViewController = [MFSideMenuContainerViewController
                              containerWithCenterViewController:navigationController
                              leftMenuViewController:nil
                              rightMenuViewController:rightMenuController];
    [self.view addSubview:sideMenuViewController.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
