//
//  main.m
//  News
//
//  Created by Dalia Abd El-Hadi on 2/5/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
